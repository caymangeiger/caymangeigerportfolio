import React, { createContext, useEffect, useState, useCallback } from 'react';

export const ScrollContext = createContext({
    scrollProgress: 0,
    setScrollProgress: () => { },
    scrollToSection: () => { },
});

export const ScrollProvider = ({ children }) => {
    const [scrollProgress, setScrollProgress] = useState(0);



    const handleScroll = useCallback(() => {
        const scrollTop = window.scrollY || document.documentElement.scrollTop;
        const scrollHeight = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        const progress = (scrollTop / scrollHeight) * 100;

        setScrollProgress(progress);
    }, []);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [handleScroll]);

    const scrollToSection = useCallback((elementId) => {
        const element = document.getElementById(elementId);
        let divTwoOffset = 175;
        let divThreeOffset = 175;
        let divFourOffset = 200;
        const isMobile = window.innerWidth <= 768;

        if (isMobile) {
            divTwoOffset -= 10;
            divThreeOffset -= 10;
        }

        if (elementId === "containerForAnimatedDivTwo") {
            const topPos = element.getBoundingClientRect().top + window.scrollY - divTwoOffset;
            window.scrollTo({ top: topPos, behavior: 'smooth' });
        } else if (elementId === "containerForAnimatedDivThree") {
            const topPos = element.getBoundingClientRect().top + window.scrollY - divThreeOffset;
            window.scrollTo({ top: topPos, behavior: 'smooth' });
        }
        else if (elementId === "containerForAnimatedDivFour") {
            const topPos = element.getBoundingClientRect().top + window.scrollY - divFourOffset;
            window.scrollTo({ top: topPos, behavior: 'smooth' });
        }
    }, []);

    const contextValue = {
        scrollProgress,
        setScrollProgress,
        scrollToSection,
    };

    return (
        <ScrollContext.Provider value={contextValue}>
            {children}
        </ScrollContext.Provider>
    );
};
