import React, { useEffect, useState, useRef } from 'react';
import { motion, useAnimation } from 'framer-motion';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'
import useWindowSize from '../functions/WindowSize';


import "./HomePage.css"

export default function HomePage() {
    const divTwoControls = useAnimation();
    const divThreeControls = useAnimation();
    const divFourControls = useAnimation();
    const [divTwoAnimationTriggered, setDivTwoAnimationTriggered] = useState(false);
    const [divThreeAnimationTriggered, setDivThreeAnimationTriggered] = useState(false);
    const [divFourAnimationTriggered, setDivFourAnimationTriggered] = useState(false);
    const [clickMeOpened, setClickMeOpened] = useState(false);
    const [skillsAnimation, setSkillsAnimation] = useState(false);
    const [projectsAnimation, setProjectsAnimation] = useState(false);
    const [aboutMeAnimation, setAboutMeAnimation] = useState(false);
    const { width } = useWindowSize()
    const videoRef = useRef(null);



    useEffect(() => {
        if (videoRef.current) {
            videoRef.current.play().catch(err => {
                console.log('Autoplay was prevented');
            });
        }
    }, []);


    useEffect(() => {
        const handleScroll = () => {
            const animatedDivTwo = document.getElementById('animatedDivTwo');
            const positionDivTwo = animatedDivTwo.getBoundingClientRect();
            const animatedDivThree = document.getElementById('animatedDivThree');
            const positionDivThree = animatedDivThree.getBoundingClientRect();
            const animatedDivFour = document.getElementById('animatedDivFour');
            const positionDivFour = animatedDivFour.getBoundingClientRect();
            const skillsHeader = document.querySelector('.betweenDivOneAndTwoHeader');
            const headerPos = skillsHeader.getBoundingClientRect();
            const projectsHeader = document.querySelector('.betweenDivTwoAndThreeHeader');
            const projectsPos = projectsHeader.getBoundingClientRect();


            // FOR ICONS AND HEADER
            if (positionDivTwo.top < window.innerHeight - 80 && positionDivTwo.bottom >= 0) {
                if (!divTwoAnimationTriggered) {
                    divTwoControls.start({ x: 0, opacity: 1 });
                    setDivTwoAnimationTriggered(true);
                    setSkillsAnimation(true);
                }
            } else {
                if (divTwoAnimationTriggered) {
                    setDivTwoAnimationTriggered(false);
                    setSkillsAnimation(false);
                    divTwoControls.start({ x: '100vw', opacity: 0 });
                }
            }

            // FOR PROJECTS AND HEADER
            if (positionDivThree.top < window.innerHeight - 80 && positionDivThree.bottom >= 0) {
                if (!divThreeAnimationTriggered) {
                    divThreeControls.start({ x: 0, opacity: 1 });
                    setDivThreeAnimationTriggered(true);
                    setProjectsAnimation(true);
                }
            } else {
                if (divThreeAnimationTriggered) {
                    setDivThreeAnimationTriggered(false);
                    setProjectsAnimation(false);
                    divThreeControls.start({ x: '-100vw', opacity: 0 });
                }
            }

            if (positionDivFour.top < window.innerHeight - 80 && positionDivFour.bottom >= 0) {
                if (!divFourAnimationTriggered) {
                    divFourControls.start({ x: 0, opacity: 1 });
                    setDivFourAnimationTriggered(true);
                    setAboutMeAnimation(true);
                }
            } else {
                if (divFourAnimationTriggered) {
                    setDivFourAnimationTriggered(false);
                    setAboutMeAnimation(false);
                    divFourControls.start({ x: '100vw', opacity: 0 });
                }
            }

        };
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [divTwoControls, divFourControls, divThreeControls, divTwoAnimationTriggered, divThreeAnimationTriggered, divFourAnimationTriggered]);


    document.querySelectorAll('.codeIconsPhotosHeader').forEach(header => {
        header.addEventListener('mouseover', function () {
            const imageId = this.id.replace('Header', '');
            const image = document.getElementById(imageId);
            if (image) {
                image.style.transform = 'scale(1.2)';
            }
        });

        header.addEventListener('mouseout', function () {
            const imageId = this.id.replace('Header', '');
            const image = document.getElementById(imageId);
            if (image) {
                image.style.transform = 'scale(1)';
            }
        });
    });

    const pictures = document.querySelectorAll('#picture1, #picture2, #picture3, #picture4, #picture5, #picture6, #picture7, #picture8, #picture9');

    pictures.forEach(picture => {
        picture.addEventListener('mouseover', () => {
            pictures.forEach(pic => {
                if (pic !== picture) pic.style.transform = 'scale(0.8)';
            });
        });
        picture.addEventListener('mouseout', () => {
            pictures.forEach(pic => pic.style.transform = 'scale(1)');
        });
    });


    return (
            <div className="homePage">
                <div className="homePageChildOne">
                    <div className="homePageChildOneDivOne">
                        <div className="homePageChildOneDivOneHeaderDiv1">
                            <div className='homePageChildOneDivOneHeaderOneDiv'>
                                <h1 className="homePageChildOneDivOneHeaderOne">
                                    Thank You For Your Interest
                                </h1>
                            </div>
                            <div className='homePageChildOneDivOneHeaderTwoDivParent'>
                                <div className='homePageChildOneDivOneHeaderTwoDivPhotoDiv'>
                                    <div className='homePageChildOneDivOneHeaderTwoDivPhotoBackground'>
                                    </div>
                                    <img
                                    className='homePageChildOneDivOneHeaderTwoDivPhoto'
                                    src='Cayman1.png'/>
                                </div>
                                <div className='homePageChildOneDivOneHeaderTwoDivDiv'>
                                    <h1 className='homePageChildOneDivOneHeaderTwoDiv'>
                                        I'm Cayman Geiger
                                    </h1>
                                    <div className='homePageChildOneDivOneDescriptionDiv'>
                                        <p className='homePageChildOneDivOneDescriptionTwoDiv'>
                                            I recently <strong>graduated</strong> from Galvanize, a prestigious coding bootcamp, since then I've <strong>successfully developed</strong> two solo projects, showcasing my proficiency as a <strong>Front-End Specialist</strong> with expertise in <strong> Next.js </strong> and <strong>React.js.</strong> As a fast learner with a passion for continual growth, I also possess <strong>full-stack development</strong> capabilities.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='homePageChildOneDivOneHeaderDiv2Parent'>
                            <div className="homePageChildOneDivOneHeaderDiv2">
                                <h1 className="homePageChildOneDivOneHeaderTwo">
                                    Get To Know Me More!
                                </h1>
                            </div>
                            <div class="scroll-indicator">
                                <FontAwesomeIcon
                                    class="dot" id="dot1"
                                    icon={faAngleDown} />
                                <FontAwesomeIcon
                                    class="dot" id="dot2"
                                    icon={faAngleDown} />
                                <FontAwesomeIcon
                                    class="dot" id="dot3"
                                    icon={faAngleDown} />
                            </div>
                        </div>
                    </div>
                    <div className='betweenDivOneAndTwoHeaderDiv'>
                        <h1
                            style={{ '--borderWidth': skillsAnimation ? '100%' : '0' }}
                            className='betweenDivOneAndTwoHeader'>
                            Skills
                        </h1>
                    </div>
                    <div className='belowBreakOut'>
                        <div id="containerForAnimatedDivTwo" style={{ overflow: 'hidden' }}>
                            <motion.div
                                id="animatedDivTwo"
                                initial={{ x: '100vw', opacity: 0 }}
                                animate={divTwoControls}
                                transition={{ type: 'spring', stiffness: 40 }}
                                className="homePageChildOneDivTwo"
                            >
                                <div className='codeIconsPhotosDiv'>
                                    <div className="codeIconsPhotoDiv">
                                        <img
                                            id="picture1"
                                            className='codeIconsPhotos'
                                            src="javascript-128.png" />
                                        <h1
                                        id="picture1Header"
                                        className='codeIconsPhotosHeader'>
                                            JavaScript
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture2"
                                            className='codeIconsPhotos'
                                            src="python-128.png" />
                                        <h1
                                        id="picture2Header"
                                        className='codeIconsPhotosHeader'>
                                            Python
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture3"
                                            className='codeIconsPhotos'
                                            src="sql-96.png" />
                                        <h1
                                        id="picture3Header"
                                        className='codeIconsPhotosHeader'>
                                            SQL
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture4"
                                            className='codeIconsPhotos'
                                            src="react-native-128.png" />
                                        <h1
                                        id="picture4Header"
                                        className='codeIconsPhotosHeader'>
                                            React
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture5"
                                            className='codeIconsPhotos'
                                            src="django-128.png" />
                                        <h1
                                        id="picture5Header"
                                        className='codeIconsPhotosHeader'>
                                            Django
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture6"
                                            className='codeIconsPhotos'
                                            src="docker-128.png" />
                                        <h1
                                        id="picture6Header"
                                        className='codeIconsPhotosHeader'>
                                            Docker
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture7"
                                            className='codeIconsPhotos'
                                            src="css-128.png" />
                                        <h1
                                        id="picture7Header"
                                        className='codeIconsPhotosHeader'>
                                            CSS
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture8"
                                            className='codeIconsPhotos'
                                            src="typescript-1001.png" />
                                        <h1
                                        id="picture8Header"
                                        className='codeIconsPhotosHeader'>
                                            Typescript
                                        </h1>
                                    </div>
                                    <div className='codeIconsPhotoDiv'>
                                        <img
                                            id="picture9"
                                            className='codeIconsPhotos'
                                            src="node-js-128.png" />
                                        <h1
                                        id="picture9Header"
                                        className='codeIconsPhotosHeader'>
                                            Node.js
                                        </h1>
                                    </div>
                                </div>
                            </motion.div>
                        </div>
                        <div className='betweenDivTwoAndThreeHeaderDiv'>
                            <h1
                                style={{ '--borderWidthProjects': projectsAnimation ? '100%' : '0' }}
                                className='betweenDivTwoAndThreeHeader'>
                                My Projects
                            </h1>
                        </div>
                        <div id="containerForAnimatedDivThree" style={{ overflow: 'hidden' }}>
                            <motion.div
                                id="animatedDivThree"
                                initial={{ x: '-100vw', opacity: 0 }}
                                animate={divThreeControls}
                                transition={{ type: 'spring', stiffness: 40 }}
                                className="homePageChildOneDivThree"
                            >
                                <div className='projectsDiv'>
                                    {width < 600 && (
                                        <h3 className='clickMe'>
                                            Click on the projects to see more!
                                        </h3>
                                    )}
                                    <div className='projectVideoDiv1'>
                                        <div className="front1">
                                            <h1 className='projectVideoHeader'>
                                                Midi
                                            </h1>
                                            <video
                                                ref={videoRef}
                                                className='projectVideo'
                                                autoPlay
                                                loop
                                                muted
                                                playsInline
                                                preload="auto"
                                                style={{ controls: false }}
                                                onCanPlay={() => videoRef.current.play()}
                                            >
                                                <source src="midiNew.mp4" type="video/mp4" />
                                            </video>
                                            <div className='projectVideoDescriptionDiv'>
                                                <div className='projectVideoDescriptionDivChildOne'>
                                                    <h3 className='projectVideoDescriptionDivChildOneHeader'>
                                                        Music Discovery
                                                    </h3>
                                                </div>
                                                <div className='projectVideoDescriptionDivChildTwo'>
                                                    <p className='projectVideoDescriptionDivChildTwoDescription'>
                                                        Our music platform is live and thriving! It's a breeze to search for artists across all genres, thanks to our artist search feature. Users can create their own Favorites List, keeping tabs on their top artists and tracks. We've made managing user accounts simple, allowing everyone to personalize their music journey. Album Cover Art Discovery - a hit for those looking for awesome wallpapers. Plus, our interface makes navigation a joy.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="back1">
                                            <div className="back1HeaderDiv">
                                                <h1 className="back1Header">
                                                    Midi
                                                </h1>
                                            </div>
                                            <div className='projectLinksDiv'>
                                            <button
                                                onClick={() => window.open("https://midi-fantastic-five1-28f57ed7ec62de863aeaf7a768417279982a32b577.gitlab.io")}
                                                className="custom-btn btn-5">
                                                <span>
                                                    Live Link
                                                </span>
                                            </button>
                                            <button
                                                onClick={() => window.open("https://gitlab.com/Fantastic-Five1/Midi")}
                                                className="custom-btn btn-5">
                                                <span>
                                                    GitHub
                                                </span>
                                            </button>
                                            </div>
                                            <div className='projectTechUsedDiv'>
                                                <h5 className='projectTechUsedHeader'>
                                                    Technology Used
                                                </h5>
                                                <ul className='projectTechUsedUl'>
                                                    <li className='projectTechUsedLi'>
                                                        Vite.js
                                                    </li>
                                                    <li className='projectTechUsedLi'>
                                                        Postgres
                                                    </li>
                                                    <li className='projectTechUsedLi'>
                                                        Fastapi
                                                    </li>
                                                    <li className='projectTechUsedLi'>
                                                        CSS
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='projectVideoDiv2'>
                                        <div className="front2">
                                            <h1 className='projectVideoHeader'>
                                                Lucid Beauty
                                            </h1>
                                            <video
                                                ref={videoRef}
                                                className='projectVideo'
                                                autoPlay
                                                loop
                                                muted
                                                playsInline
                                                preload="auto"
                                                style={{ controls: false }}
                                                onCanPlay={() => videoRef.current.play()}
                                            >
                                                <source src="lucidNew.mp4" type="video/mp4" />
                                            </video>
                                            <div className='projectVideoDescriptionDiv'>
                                                <div className='projectVideoDescriptionDivChildOne'>
                                                    <h3 className='projectVideoDescriptionDivChildOneHeader'>
                                                        Health And Wellness
                                                    </h3>
                                                </div>
                                                <div className='projectVideoDescriptionDivChildTwo'>
                                                    <p className='projectVideoDescriptionDivChildTwoDescription'>
                                                        Discover relaxation and wellness with our health-focused website, expertly designed for easy service search and booking. Fully integrated with JWT token technology for secure sessions, it lets clients effortlessly find and schedule various esthetician services. Our advanced navigation menu, complete with social media links, ensures a seamless, enriching online experience. Perfect for those seeking services to fit their lifestyle.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <div className="back2">
                                        <div className="back1HeaderDiv">
                                            <h1 className="back1Header">
                                                Lucid Beauty
                                            </h1>
                                        </div>
                                        <div className='projectLinksDiv'>
                                            <button
                                                onClick={() => window.open("https://gitlab.com/caymangeiger/hannahs-esthetician-website-final")}
                                                className="custom-btn btn-5">
                                                <span>
                                                    GitHub
                                                </span>
                                            </button>
                                        </div>
                                        <div className='projectTechUsedDiv'>
                                            <h5 className='projectTechUsedHeader'>
                                                Technology Used
                                            </h5>
                                            <ul className='projectTechUsedUl'>
                                                <li className='projectTechUsedLi'>
                                                    Next.js
                                                </li>
                                                <li className='projectTechUsedLi'>
                                                    Django REST
                                                </li>
                                                <li className='projectTechUsedLi'>
                                                    AWS
                                                </li>
                                                <li className='projectTechUsedLi'>
                                                    CSS
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    </div>
                                    <div className='projectVideoDiv3'>
                                        <div className="front3">
                                            <h1 className='projectVideoHeader'>
                                                SoleMate
                                            </h1>
                                            <video
                                                ref={videoRef}
                                                className='projectVideo'
                                                autoPlay
                                                loop
                                                muted
                                                playsInline
                                                preload="auto"
                                                style={{ controls: false }}
                                                onCanPlay={() => videoRef.current.play()}
                                            >
                                                <source src="shoeNew.mp4" type="video/mp4" />
                                            </video>
                                            <div className='projectVideoDescriptionDiv'>
                                                <div className='projectVideoDescriptionDivChildOne'>
                                                    <h3 className='projectVideoDescriptionDivChildOneHeader'>
                                                        Shoe Wardrobe
                                                    </h3>
                                                </div>
                                                <div className='projectVideoDescriptionDivChildTwo'>
                                                    <p className='projectVideoDescriptionDivChildTwoDescription'>
                                                        Explore our custom shoe shopping site, crafted for easy browsing and purchasing. Advanced search lets users filter by style, size, and brand. Our user account system saves shopping history and preferences. Enjoy a streamlined shopping cart and a Favorites feature for your top picks. Enhanced with secure login/logout for data safety, this site is ideal for finding the perfect pair to match your style.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    <div className="back3">
                                        <div className="back1HeaderDiv">
                                            <h1 className="back1Header">
                                                SoleMate
                                            </h1>
                                        </div>
                                        <div className='projectLinksDiv'>
                                            {/* <button
                                                onClick={() => scrollToSection('containerForAnimatedDivTwo')}
                                                className="custom-btn btn-5">
                                                <span>
                                                    Live Link
                                                </span>
                                            </button> */}
                                            <button
                                                onClick={() => window.open("https://gitlab.com/caymangeiger/shoe-website")}
                                                className="custom-btn btn-5">
                                                <span>
                                                    GitHub
                                                </span>
                                            </button>
                                        </div>
                                        <div className='projectTechUsedDiv'>
                                            <h5 className='projectTechUsedHeader'>
                                                Technology Used
                                            </h5>
                                            <ul className='projectTechUsedUl'>
                                                <li className='projectTechUsedLi'>
                                                    React.js
                                                </li>
                                                <li className='projectTechUsedLi'>
                                                    Django
                                                </li>
                                                <li className='projectTechUsedLi'>
                                                    Web Api
                                                </li>
                                                <li className='projectTechUsedLi'>
                                                    CSS
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </motion.div>
                        </div>
                        <div className='betweenDivThreeAndFourHeaderDiv'>
                            <h1
                                style={{ '--borderWidth': aboutMeAnimation ? '100%' : '0' }}
                                className='betweenDivThreeAndFourHeader'>
                                About Me
                            </h1>
                        </div>
                        <div id="containerForAnimatedDivFour" style={{ overflow: 'hidden' }}>
                            <motion.div
                                id="animatedDivFour"
                                initial={{ x: '100vw', opacity: 0 }}
                                animate={divFourControls}
                                transition={{ type: 'spring', stiffness: 40 }}
                                className="homePageChildOneDivFour"
                            >
                                <div className='aboutMeMainDiv'>
                                    <div className='aboutMeMainDivChildOne'>
                                        <h1 className='aboutMeStoryHeader'>
                                            My Story
                                        </h1>
                                        <p className='aboutMeStoryInfo'>
                                        Over four years in sales, I consistently ranked as a top performer, honing my interpersonal skills and understanding of client needs. My passion for technology led me to web development, where I excelled in coding and quickly mastered multiple frameworks. As class speaker in my school, I demonstrated dedication to coding and teaching others. Now, I'm eager to combine my sales experience, tech expertise, and love for teaching in a challenging new role that fosters growth.
                                        </p>
                                    </div>
                                    <div className='aboutMeMainDivChildTwo'>
                                        <div className='aboutMeLanguagesDiv'>
                                            <h1 className='aboutMeHeaders'>
                                                Languages
                                            </h1>
                                            <ul className='aboutMeLanguagesUl'>
                                                <li>
                                                    Python
                                                </li>
                                                <li>
                                                    Javascript
                                                </li>
                                                <li>
                                                    PHP
                                                </li>
                                                <li>
                                                    Typescript
                                                </li>
                                            </ul>
                                        </div>
                                        <div className='aboutMeFrontEndDiv'>
                                            <h1 className='aboutMeHeaders'>
                                                Front End
                                            </h1>
                                            <ul className='aboutMeFrontEndUl'>
                                                <li>
                                                    Next.js
                                                </li>
                                                <li>
                                                    Vite.js
                                                </li>
                                                <li>
                                                    React.js
                                                </li>
                                                <li>
                                                    Solid.js
                                                </li>
                                            </ul>
                                        </div>
                                        <div className='aboutMeBackEndDiv'>
                                            <h1 className='aboutMeHeaders'>
                                                Back End
                                            </h1>
                                            <ul className='aboutMeBackEndUl'>
                                                <li>
                                                    Laravel
                                                </li>
                                                <li>
                                                    Prisma
                                                </li>
                                                <li>
                                                    Django
                                                </li>
                                                <li>
                                                    FastAPI
                                                </li>
                                                <li>
                                                    Node.js
                                                </li>
                                            </ul>
                                        </div>
                                        <div className='aboutMeDatabasesDiv'>
                                            <h1 className='aboutMeHeaders'>
                                                Databases
                                            </h1>
                                            <ul className='aboutMeDatabasesUl'>
                                                <li>
                                                    SQL
                                                </li>
                                                <li>
                                                    Postgres
                                                </li>
                                                <li>
                                                    NoSQL
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </motion.div>
                        </div>
                    </div>
                </div>
                <div className="homePageChildTwo">

                </div>
                <img
                    className="backGroundPhotoPortfolio"
                    src="backGroundPhotoPortfolio.jpg" />
                <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
                <div className='linksIcons'>
                    <h4 style={{ textAlign: "center" }}>
                        <a style={{ color: "pink" }}>Icons</a>  by <a style={{ color: "pink" }} target="_blank" href="https://icons8.com">Icons8</a>
                    </h4>
                </div>
                </div>
            </div>
    )
}
