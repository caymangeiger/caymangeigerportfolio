import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./nav/Nav"
import HomePage from "./home/HomePage"
import './App.css'
import { ScrollProvider } from './nav/ScrollContext';

function App() {

  return (
    <BrowserRouter>
      <ScrollProvider>
      <Nav />
      <Routes>
        <Route path="/">
          <Route index element={<HomePage />} />
        </Route>
      </Routes>
      </ScrollProvider>
    </BrowserRouter>
  )
}

export default App
